import express from "express"
import controllerRoute from "./controller/routes.js"

const app = express()
app.use(express.json())


controllerRoute(app)

app.listen(3000)