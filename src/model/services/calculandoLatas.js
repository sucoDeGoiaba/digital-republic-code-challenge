// Calcula quantidade de latas necessárias
const calculandoLatas = (area) =>{
    let litrosDeTinta = Math.ceil(area/5)
    
    const latasUsadas = {
        "litrosUsados" : litrosDeTinta,
        "litros18" : 0,
        "litros3" : 0,
        "litros2" : 0,
        "litros0" : 0
    }

    while(litrosDeTinta !== 0){
        if (litrosDeTinta - 18 >= 0) {
            litrosDeTinta -= 18
            latasUsadas.litros18++
        }

        if (litrosDeTinta - 3.6 >= 0) {
            litrosDeTinta -= 3.6
            latasUsadas.litros3++
        }

        if (litrosDeTinta - 2.5 >= 0) {
            litrosDeTinta -= 2.5
            latasUsadas.litros2++
        }

        if (litrosDeTinta - 0.5 >= 0) {
            litrosDeTinta -= 0.5
            latasUsadas.litros0++
        }
    }

    return latasUsadas
}

export default calculandoLatas