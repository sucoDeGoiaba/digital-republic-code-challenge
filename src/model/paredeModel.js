import validandoArea from "./services/validaArea.js"
import validandoPrtJnl from "./services/validaPrtJnl.js"
import validandoAltura from "./services/validaAltura.js"
import calculandoLatas from "./services/calculandoLatas.js"

const paredeModel = (input) => {
    let areaPintavel = 0
    // Validação referente as parede
    for (const key in input) {
        validandoAltura(input[key].altura)
        validandoArea(input[key].largura, input[key].altura)
        areaPintavel += input[key].largura * input[key].altura
    }

    // Validação referente as portas e janela
    for (const key in input) {
        validandoPrtJnl(input[key].qntPrt, input[key].qntJnl, areaPintavel)
        areaPintavel -= ((0.80 * 1.90) * input[key].qntPrt) + ((2.00 * 1.20) * input[key].qntJnl)
    }

    // Recebe calculo da quantidade de latas
    const latas = calculandoLatas(areaPintavel)
    return latas
}

export default paredeModel