// Checa se a área das paredes está entre 1 e 15 metros quadrados
const validandoArea = (base, altura) => {
    if (base * altura >= 1 && base * altura <= 15) {
        return true
    } else {
        throw new Error("Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados")
    }
}

export default validandoArea