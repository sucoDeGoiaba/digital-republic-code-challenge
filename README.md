# Digital Republic Code Challenge:
## Resumo: 
Uma aplicação da qual passado parâmetros como, base e altura de uma parede, é retornado a quantidade de latas necessárias para cobrir toda a parede, levando em conta portas e janelas.

## Pré-Requisitos:

* Node.js  v.16.14.0
* NPM v.8.3.1

## Pacotes utilizados
* [Express](https://www.npmjs.com/package/express) v.4.17.3

## Instalação da Aplicação
Abra o terminal/Powershell e rode os comandos abaixo:

Clonando o repositório:

Via HTTPS:
```
git clone https://gitlab.com/sucoDeGoiaba/digital-republic-code-challenge.git
```

Via SSH:
```
git clone git@gitlab.com:sucoDeGoiaba/digital-republic-code-challenge.git
```

Entrando na pasta:
```
cd digital-republic-code-challenge
```

Instalando dependências: 
```
npm install
```

Iniciando o servidor:
```
npm start
```

---
## Rota implementada:

 * **POST /**

   Schema da requisição
    ```
    {
        "parede1": {
            "altura" : <Float>
            "largura" : <Float>
            "qntPrt" : <Integer>
            "qntJnl" : <Integer>
        },
        "parede2": {
            "altura" : <Float>
            "largura" : <Float>
            "qntPrt" : <Integer>
            "qntJnl" : <Integer>
        },
        "parede3": {
            "altura" : <Float>
            "largura" : <Float>
            "qntPrt" : <Integer>
            "qntJnl" : <Integer>
        },
        "parede4": {
            "altura" : <Float>
            "largura" : <Float>
            "qntPrt" : <Integer>
            "qntJnl" : <Integer>
        }
    }
    ```
 
    Schema da resposta
    ```
    {
        "litrosUsados": <Integer>,
        "litros18": <Integer>,
        "litros3": <Integer>,
        "litros2": <Integer>,
        "litros0": <Integer>
    }
    ```
