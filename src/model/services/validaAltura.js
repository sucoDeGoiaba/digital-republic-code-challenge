// Checa se a parede é 30cm maior que a altura da porta (1.90)
const validandoAltura = (altura)=>{
    if (altura < 1.90 + 0.30) {
        throw new Error("A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta")
    } else {
        return true
    }
}

export default validandoAltura