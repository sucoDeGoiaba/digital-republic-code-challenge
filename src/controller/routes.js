import paredeModel from "../model/paredeModel.js"

const controllerRoute = (app) => {
    app.post('/', (req, res)=>{
        const body = req.body
        const latas = paredeModel(body)
        res.json(latas)
    })
}

export default controllerRoute
