// Checa se a área total de portas e janelas é menor que 50% da área de paredes
const validandoPrtJnl = (qntPrt, qntJnl, areaParedes) => {
    if (((2.00 * 1.20) * qntJnl) + ((0.80 * 1.90) * qntPrt) <= (areaParedes * 50) / 100) {
        return true
    } else {
        throw new Error("O total de área das portas e janelas deve ser no máximo 50% da área de parede")
    }
}

export default validandoPrtJnl